//
//  BetterRest.swift
//  100daysofswiftui
//
//  Created by hiba on 29/8/2022.
//

import CoreML
import SwiftUI

struct BetterRest: View {
    @State private var wakeUp = defaultWakeTime
    @State private var sleepAmount = 8.0
    @State private var coffeeAmount = 1

    //@State private var alertTitle = ""
    @State private var alertMessage = ""
   // @State private var showingAlert = false
    
  

    var body: some View {
        NavigationView {
            VStack {
                Form {
                    Section{
                        DatePicker("Please enter a time", selection: $wakeUp, displayedComponents: .hourAndMinute)
                            .labelsHidden()
                    }header: {
                            Text("When do you want to wake up?")
                            .font(.headline)
                        
                    }

                    Section{
                   

                        Stepper("\(sleepAmount.formatted()) hours", value: $sleepAmount, in: 4...12, step: 0.25)
                    }header: {
                        Text("Desired amount of sleep")
                            .font(.headline)
                    
                }

                    Section{
                       
                            Picker("number of cups",selection: $coffeeAmount){
                                ForEach(1 ..< 21){ cup in
                                 
                                    Text(cup == 1 ? "1 cup" : "\(cup) cups")
                               

                                }
                            }
                            
                                              
                        
                    }
                header: {
                    Text("Daily coffee intake")
                        .font(.headline)
                
            }
                    Text("Your ideal bedtime is : \(calculateBedtime())")
                        .font(.title2)
                        .fontWeight(.bold)
                    
                }
                
                
       
         
            }
            .navigationTitle("BetterRest")
           /* .toolbar {
                Button("Calculate", action: calculateBedtime)
            }
            .alert(alertTitle, isPresented: $showingAlert) {
                Button("OK") { }
            } message: {
                Text(alertMessage)
            }*/
        }
    }

    
    static var defaultWakeTime: Date {
        var components = DateComponents()
        components.hour = 7
        components.minute = 0
        return Calendar.current.date(from: components) ?? Date.now
    }
    func calculateBedtime() -> String {
        do {
            let config = MLModelConfiguration()
            let model = try SleepCalculator(configuration: config)

            let components = Calendar.current.dateComponents([.hour, .minute], from: wakeUp)
            let hour = (components.hour ?? 0) * 60 * 60
            let minute = (components.minute ?? 0) * 60

            let prediction = try model.prediction(wake: Double(hour + minute), estimatedSleep: sleepAmount, coffee: Double(coffeeAmount))

            let sleepTime = wakeUp - prediction.actualSleep
          //  alertTitle = "Your ideal bedtime is…"
            //alertMessage = sleepTime.formatted(date: .omitted, time: .shortened)
            return sleepTime.formatted(date: .omitted, time: .shortened)
        } catch {
            return "Error"
           // alertTitle = "Error"
           // alertMessage = "Sorry, there was a problem calculating your bedtime."
        }

     //   showingAlert = true
    }

}
struct BetterRest_Previews: PreviewProvider {
    static var previews: some View {
        BetterRest()
    }
}
