//
//  day10:11.swift
//  100daysofswiftui
//
//  Created by hiba on 26/8/2022.
//

import SwiftUI

struct day10_11: View {
    var body: some View {
        ZStack {
      
            VStack (spacing : 70){
                VStack(spacing: 8){
                Text("#100DaysOfSwiftUi")
                        .font(.system(size: 40))
                        .fontWeight(.bold)
                        .foregroundColor(Color(hue: 0.552, saturation: 0.942, brightness: 0.931, opacity: 0.662))
                    HStack {
                        Image(systemName: "star.fill")
                        Text("Day10/11")
                            .font(.system(size: 40))
                        Image(systemName: "star.fill")
                    }
                }
                Image(systemName: "swift")
                    .font(.system(size: 150))
                    .foregroundColor(Color(hue: 0.552, saturation: 0.942, brightness: 0.931, opacity: 0.662))
          
                       
                    Text("""
        structs,
        computed properties,
        property observers,
        access control,
        static properties and methods
        """)
                    .font(.system(size: 30))
                    .multilineTextAlignment(.center)
                    .lineSpacing(4)
                    
                
            }
        }
    }
}

struct day10_11_Previews: PreviewProvider {
    static var previews: some View {
        day10_11()
    }
}
