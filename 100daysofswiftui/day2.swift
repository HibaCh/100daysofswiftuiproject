//
//  day2.swift
//  100daysofswiftui
//
//  Created by hiba on 10/8/2022.
//

import SwiftUI

struct day2: View {
    var body: some View {
        ZStack {
      
            VStack (spacing : 70){
                VStack(spacing: 8){
                Text("#100DaysOfSwiftUi")
                        .font(.system(size: 40))
                        .fontWeight(.bold)
                        .foregroundColor(Color(hue: 0.927, saturation: 0.544, brightness: 0.909))
                    HStack {
                        Image(systemName: "star.fill")
                        Text("Day2")
                            .font(.system(size: 40))
                        Image(systemName: "star.fill")
                    }
                }
                Image(systemName: "swift")
                    .font(.system(size: 150))
                    .foregroundColor(Color(hue: 0.927, saturation: 0.544, brightness: 0.909))
          
                       
                    Text("""
        Booleans,
        string interpolation
        """)
                    .font(.system(size: 30))
                    .multilineTextAlignment(.center)
                    .lineSpacing(/*@START_MENU_TOKEN@*/5.0/*@END_MENU_TOKEN@*/)
                    
                
            }
        }
    }
}

struct day2_Previews: PreviewProvider {
    static var previews: some View {
        day2()
    }
}
