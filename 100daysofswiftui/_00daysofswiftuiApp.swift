//
//  _00daysofswiftuiApp.swift
//  100daysofswiftui
//
//  Created by hiba on 10/8/2022.
//

import SwiftUI

@main
struct _00daysofswiftuiApp: App {
    var body: some Scene {
        WindowGroup {
            ContentViewMoonshot()
        }
    }
}

/*struct BookwormApp: App {
 @StateObject private var dataController = DataController()

 var body: some Scene {
     WindowGroup {
         BookwormContentView()
             .environment(\.managedObjectContext, dataController.container.viewContext)
     }
 }
}*/
