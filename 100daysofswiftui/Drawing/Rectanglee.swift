//
//  Rectangle.swift
//  100daysofswiftui
//
//  Created by hiba on 9/9/2022.
//

import SwiftUI
struct ColorCyclingRectangle : View{
    var amount = 0.0
    var steps = 100
    var body: some View{
        ZStack{
            ForEach(0..<steps){
                value in
                Rectangle()
                    .inset(by: CGFloat(value))
                    .strokeBorder(LinearGradient(gradient: Gradient(colors: [self.color(for:value , brightness:1), self.color(for:value, brightness:0.5)]), startPoint: .top, endPoint: .bottom),lineWidth:2)
            }
        }
        .drawingGroup()
    }
    
    
    
    func color(for value: Int, brightness : Double)->Color{
        var targetHue = Double(value) / Double(self.steps) + self.amount
        if targetHue > 1 {
            targetHue -= 1
        }
        return Color(hue:targetHue,saturation:1 , brightness: brightness)
    }
    
}
struct Rectanglee: View {
    @State private var colorCycle = 0.0
    var body: some View {
        NavigationView {
            VStack {
                ColorCyclingCircle(amount: self.colorCycle)
                    .frame(width:250 , height: 250)
                Slider(value: $colorCycle)
                    .padding(.bottom,50)
            }
        }
        
    }
}

struct Rectanglee_Previews: PreviewProvider {
    static var previews: some View {
        Rectanglee()
    }
}
