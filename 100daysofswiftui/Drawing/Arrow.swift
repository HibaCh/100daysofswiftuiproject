//
//  Arrow.swift
//  100daysofswiftui
//
//  Created by hiba on 9/9/2022.
//

import SwiftUI

struct Arrooow:Shape{
    func path(in rect : CGRect)-> Path {
        var path = Path()
        path.move(to: CGPoint(x: rect.midX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY/4 ))
        path.addLine(to: CGPoint(x: rect.maxX/3, y: rect.maxY/4 ))
        path.addLine(to: CGPoint(x: rect.maxX/3, y: rect.maxY ))
        path.addLine(to: CGPoint(x: rect.maxX/1.5, y: rect.maxY ))
        path.addLine(to: CGPoint(x: rect.maxX/1.5, y: rect.maxY/4 ))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY/4 ))
        path.addLine(to: CGPoint(x: rect.midX, y: rect.minY ))

        return path
    }
}

struct Arrow: View {
    @State private var lineWidth = 5.0
    @State private var isShowBoldLine = false
    var body: some View {
        NavigationView{
            VStack{
                Arrooow()
                    .stroke(Color.blue, style:StrokeStyle(lineWidth:CGFloat(self.isShowBoldLine ? 20.0 : self.lineWidth),lineCap: .round,lineJoin: .round))
                    .frame(width:150,height:350)
                    .onTapGesture {
                        
                            withAnimation{
                                self.isShowBoldLine.toggle()
                            }
                        
                    }
            }
        }
    }
}

struct Arrow_Previews: PreviewProvider {
    static var previews: some View {
        Arrow()
    }
}
