//
//  WordScramble.swift
//  100daysofswiftui
//
//  Created by hiba on 30/8/2022.
//

import SwiftUI


struct DrawText :ViewModifier {
    let font = Font.system(size: 22 , weight: .heavy)
    func body(content: Content) -> some View {
        content
            .font(font)
    }
}
struct DrawHorzText : View {
    var text: String
    var textResult : String
    var body: some View{
        HStack{
            Text(text)
                .modifier(DrawText())
            Text(textResult)
                .modifier(DrawText())
        }
    }
}

struct WordScramble: View {
    @State private var score = 0
    @State private var usedWords = [String]()
    @State private var allWords = [String]()
    @State private var rootWord = ""
    @State private var newWord = ""
    
    @State private var errorTitle = ""
    @State private var errorMessage = ""
    @State private var showingError = false
    
    
    
    var body: some View {
        NavigationView {
            List {
                HStack{
                Section {
                    TextField("Enter your word", text: $newWord)
                        .autocapitalization(.none)
                }
                    Button("NewWord", action: NewWord)
                }
                
               
                Section {
                    ForEach(usedWords, id: \.self) { word in
                        HStack {
                            Image(systemName: "\(word.count).circle")
                            Text(word)
                        }
                    }
                  
                        
                }
                
           
                DrawHorzText(text: "Your score :", textResult: "\(score)")
            }
            .navigationTitle(rootWord)
       
            .onSubmit(addNewWord)
            .onAppear(perform: startGame)
            .toolbar {
                 Button("startGame", action: startGame)
             }
            .alert(errorTitle, isPresented: $showingError) {
                Button("OK", role: .cancel) { }
            } message: {
                Text(errorMessage)
            }
            
        }
    }

    
    
    
    
    func addNewWord() {
        let answer = newWord.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        guard answer.count > 0 else { return }

        guard isShorter(word: answer) else {
            wordError(title: "Word used is short", message: "let it be more longer")
            return
        }
        
        guard isOriginal(word: answer) else {
            wordError(title: "Word used already", message: "Be more original!")
            return
        }

        guard isPossible(word: answer) else {
            wordError(title: "Word not possible", message: "You can't spell that word from '\(rootWord)'!")
            return
        }

        guard isReal(word: answer) else {
            wordError(title: "Word not recognized", message: "You can't just make them up, you know!")
            return
        }

        withAnimation {
            usedWords.insert(answer, at: 0)
        }
     score += 1
        newWord = ""
    }

    func startGame() {
        if let startWordsURL = Bundle.main.url(forResource: "start", withExtension: "txt") {
            if let startWords = try? String(contentsOf: startWordsURL) {
                allWords = startWords.components(separatedBy: "\n")
                rootWord = allWords.randomElement() ?? "silkworm"
                usedWords = []
                score = 0
                return
            }
        }

        fatalError("Could not load start.txt from bundle.")
    }
    func NewWord(){
        rootWord = allWords.randomElement() ??  "silkworm"
    }
    
    func isShorter(word: String) -> Bool {
        word.count > 3
    }


    func isOriginal(word: String) -> Bool {
        !usedWords.contains(word)
    }

    func isPossible(word: String) -> Bool {
        var tempWord = rootWord

        for letter in word {
            if let pos = tempWord.firstIndex(of: letter) {
                tempWord.remove(at: pos)
            } else {
                return false
            }
        }

        return true
    }

    func isReal(word: String) -> Bool {
        let checker = UITextChecker()
        let range = NSRange(location: 0, length: word.utf16.count)
        let misspelledRange = checker.rangeOfMisspelledWord(in: word, range: range, startingAt: 0, wrap: false, language: "en")

        return misspelledRange.location == NSNotFound
    }

    func wordError(title: String, message: String) {
        errorTitle = title
        errorMessage = message
        showingError = true
    }
}

struct WordScramble_Previews: PreviewProvider {
    static var previews: some View {
        WordScramble()
    }
}
