//
//  Order2.swift
//  100daysofswiftui
//
//  Created by hiba on 15/9/2022.
//

import SwiftUI 

import SwiftUI

class MyOrder: ObservableObject{
 @Published var stractOrder = StractOrder()
}


struct StractOrder:  Codable {

    static let types = ["Vanilla", "Strawberry", "Chocolate", "Rainbow"]

   var type = 0
   var quantity = 3

     var specialRequestEnabled = false {
        didSet {
            if specialRequestEnabled == false {
                extraFrosting = false
                addSprinkles = false
            }
        }
    }

   var extraFrosting = false
   var addSprinkles = false

    var name = ""
   var streetAddress = ""
  var city = ""
     var zip = ""

    var hasValidAddress: Bool {
        if name.isEmpty || streetAddress.isEmpty || city.isEmpty || zip.isEmpty {
            return false
        }
        if (!streetAddress.contains("@") || !streetAddress.contains(".")) {
            return false
        }
        if (name.count < 4)  {
            return false
        }
        
        if name.contains(" ") || streetAddress.contains(" ") || city.contains(" ") || zip.contains(" ") {
            return false
        }
        
        
        return true
    }

    var cost: Double {
        // $2 per cake
        var cost = Double(quantity) * 2

        // complicated cakes cost more
        cost += (Double(type) / 2)

        // $1/cake for extra frosting
        if extraFrosting {
            cost += Double(quantity)
        }

        // $0.50/cake for sprinkles
        if addSprinkles {
            cost += Double(quantity) / 2
        }

        return cost
    }

}
