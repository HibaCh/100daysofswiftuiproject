//
//  AddressView2.swift
//  100daysofswiftui
//
//  Created by hiba on 15/9/2022.
//

import SwiftUI

struct AddressView2: View {
    @ObservedObject var order: MyOrder

    var body: some View {
        Form {
            Section {
                TextField("Name", text: $order.stractOrder.name)
                TextField("Street address", text: $order.stractOrder.streetAddress)
                TextField("City", text: $order.stractOrder.city)
                TextField("Zip", text: $order.stractOrder.zip)
            }

            Section {
                NavigationLink {
                    CheckoutView2(order: order)
                } label: {
                    Text("Check out")
                }
            }
            .disabled(order.stractOrder.hasValidAddress == false)
            
       
        }
        .navigationTitle("Delivery details")
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct AddressView2_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            AddressView2(order: MyOrder())
        }
    }
}
