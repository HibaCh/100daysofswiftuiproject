//
//  CheckoutView2.swift
//  100daysofswiftui
//
//  Created by hiba on 15/9/2022.
//


import SwiftUI

struct CheckoutView2: View {
    @ObservedObject var order: MyOrder

    @State private var confirmationMessage = ""
    @State private var showingConfirmation = false
    @State private var err = false
    @State private var errMessage = ""
    var body: some View {
        ScrollView {
            VStack {
                AsyncImage(url: URL(string: "https://hws.dev/img/cupcakes@3x.jpg"), scale: 3) { image in
                    image
                        .resizable()
                        .scaledToFit()
                } placeholder: {
                    ProgressView()
                }
                .frame(height: 233)

                Text("Your total is \(order.stractOrder.cost, format: .currency(code: "USD"))")
                    .font(.title)

                Button("Place Order") {
                    Task {
                        await placeOrder()
                    }
                }
                .padding()
                
                if err{
                Text(errMessage)
                    .foregroundColor(.red)
                }
            }
        }
        .navigationTitle("Check out")
        .navigationBarTitleDisplayMode(.inline)
        .alert("Thank you!", isPresented: $showingConfirmation) {
            Button("OK") { }
        } message: {
            Text(confirmationMessage)
        }
    }

    func placeOrder() async {
        guard let encoded = try? JSONEncoder().encode(order.stractOrder) else {
            print("Failed to encode order")
            return
        }

        let url = URL(string: "https://reqres.in/api/cupcakes")!
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"

        do {
            let (data, _) = try await URLSession.shared.upload(for: request, from: encoded)

            let decodedOrder = try JSONDecoder().decode(StractOrder.self, from: data)
            confirmationMessage = "Your order for \(decodedOrder.quantity)x \(StractOrder.types[decodedOrder.type].lowercased()) cupcakes is on its way!"
            showingConfirmation = true
        } catch {
            errMessage = "Checkout failed. \(error.localizedDescription) "
            err = true
            print("Checkout failed.")
        }
    }
}

struct CheckoutView2_Previews: PreviewProvider {
    static var previews: some View {
        CheckoutView2(order: MyOrder())
    }
}
