//
//  CupcakeCornerContentView2.swift
//  100daysofswiftui
//
//  Created by hiba on 15/9/2022.
//

import SwiftUI

struct CupcakeCornerContentView2: View {
    @StateObject var order = MyOrder()

    var body: some View {
        NavigationView {
            Form {
                Section {
                    Picker("Select your cake type", selection: $order.stractOrder.type) {
                        ForEach(StractOrder.types.indices) {
                            Text(Order.types[$0])
                        }
                    }

                    Stepper("Number of cakes: \(order.stractOrder.quantity)", value: $order.stractOrder.quantity, in: 3...20)
                }

                Section {
                    Toggle("Any special requests?", isOn: $order.stractOrder.specialRequestEnabled.animation())

                    if order.stractOrder.specialRequestEnabled {
                        Toggle("Add extra frosting", isOn: $order.stractOrder.extraFrosting)
                        Toggle("Add extra sprinkles", isOn: $order.stractOrder.addSprinkles)
                    }
                }

                Section {
                    NavigationLink {
                        AddressView2(order: order)
                    } label: {
                        Text("Delivery details")
                    }
                }
            }
            .navigationTitle("Cupcake Corner")
        }
    }
}

struct CupcakeCornerContentView2_Previews: PreviewProvider {
    static var previews: some View {
        CupcakeCornerContentView2()
    }
}
