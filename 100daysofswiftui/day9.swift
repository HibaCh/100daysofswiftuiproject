//
//  day9.swift
//  100daysofswiftui
//
//  Created by hiba on 24/8/2022.
//

import SwiftUI

struct day9: View {
    var body: some View {
        ZStack {
      
            VStack (spacing : 70){
                VStack(spacing: 8){
                Text("#100DaysOfSwiftUi")
                        .font(.system(size: 40))
                        .fontWeight(.bold)
                        .foregroundColor(Color(hue: 0.978, saturation: 0.942, brightness: 0.931, opacity: 0.831))
                    HStack {
                        Image(systemName: "star.fill")
                        Text("Day9")
                            .font(.system(size: 40))
                        Image(systemName: "star.fill")
                    }
                }
                Image(systemName: "swift")
                    .font(.system(size: 150))
                    .foregroundColor(Color(hue: 0.978, saturation: 0.942, brightness: 0.931, opacity: 0.831))
          
                       
                    Text("""
        closures,
        and passing functions into functions
        """)
                    .font(.system(size: 30))
                    .multilineTextAlignment(.center)
                    .lineSpacing(4)
                    
                
            }
        }
    }
}

struct day9_Previews: PreviewProvider {
    static var previews: some View {
        day9()
    }
}
