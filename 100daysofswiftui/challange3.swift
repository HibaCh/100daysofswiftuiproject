//
//  challange3.swift
//  100daysofswiftui
//
//  Created by hiba on 26/8/2022.
//

import SwiftUI

struct challange3: View {
    @State private var scoreTitle = ""
    @State private var yourTransformers = 0
    @State private var showingScore = false
    @State private var RPS = ["🪨", "📄",  "✂️"].shuffled()
    @State private var StateGame = ["🎖 win 🎖" , "lose😢"].shuffled()
    @State private var scoreNb = 0
    
    func WinLose (prps:String) {
        yourTransformers += 1
        if yourTransformers != 9 {
        
        if RPS[1].contains("🪨"){
            if StateGame[1].contains("win") {
                if prps.contains("✂️"){
                    scoreNb += 1
           
                }else{
                    scoreNb += 0
           
                }
            }
            else if StateGame[1].contains("lose") {
                if prps.contains("📄"){
                    scoreNb += 1
           
                }else{
                    scoreNb += 0
           
                }
            }
            else{
                scoreNb += 0
            }
        }
        
        if RPS[1].contains("📄"){
            if StateGame[1].contains("win") {
                if prps.contains("🪨"){
                    scoreNb += 1
           
                }else{
                    scoreNb += 0
           
                }
            }
            else if StateGame[1].contains("lose") {
                if prps.contains("✂️"){
                    scoreNb += 1
           
                }else{
                    scoreNb += 0
           
                }
            }
            else{
                scoreNb += 0
       
            }
        }
        
        if RPS[1].contains("✂️"){
            if StateGame[1].contains("win") {
                if prps.contains("📄"){
                    scoreNb += 1
           
                }else{
                    scoreNb += 0
           
                }
            }
            else if StateGame[1].contains("lose") {
                if prps.contains("🪨"){
                    scoreNb += 1
           
                }else{
                    scoreNb += 0
           
                }
            }
            else{
                scoreNb += 0
       
            }
        }
        }else{
            if scoreNb >= 4 {
                scoreTitle = "you win \(scoreNb)/8 "
            }else{
                scoreTitle = "you lose \(scoreNb)/8 "
            }
            showingScore = true
        }
      
    }
    
    var body: some View {
        ZStack {
            Color.red
                .opacity(0.3)
                .edgesIgnoringSafeArea(.all)
            VStack(spacing:40){
                
                Spacer()
                
                Text("RPS Game")
                   
                    .font(.system(size: 50))
                    .fontWeight(.thin)
                    .foregroundColor(Color.red)
                    .italic()
                  
            
                Text(RPS[1])
                    .font(.system(size: 40))
                    .frame(width:70 , height:70)
                        .padding()
                        .background(.regularMaterial)
                        .clipShape(RoundedRectangle(cornerRadius: 50))
                        .font(.system(size: 25))
                
                 
                HStack{
                    Text("you must :")
                        .font(.system(size: 30))
                    Text(StateGame[1])
                            .font(.system(size: 40))
                }

                 HStack{
                                ForEach(RPS, id: \.self){ rps in
                                    Button {
                                        WinLose(prps: rps)
                                        RPS.shuffle()
                                        StateGame.shuffle()
                                    } label: {
                                        Text(rps)
                                            .font(.system(size: 40))
                                            .frame(width:70 , height:70)
                                                .padding()
                                                .background(.regularMaterial)
                                                .clipShape(RoundedRectangle(cornerRadius: 50))
                                                .font(.system(size: 25))
                                    }
                                   
                                 
                                  
                         
                                }
                
                            
                    }
                  
                    Spacer()
                  Spacer()
                    
                
                Text("your score : \(scoreNb)")
                    .font(.system(size: 30))
                    .fontWeight(.light)
                    .foregroundColor(Color.red)
                    .italic()
                Spacer()
                
                }
            
        }
        .alert(scoreTitle, isPresented: $showingScore) {
            if yourTransformers > 8 {
                Button("Reset", action: ResetFunc)
            }
        }
    }
    

    
    func ResetFunc () {
        RPS.shuffle()
        StateGame.shuffle()
        yourTransformers = 0
        scoreNb = 0
    }
}

struct challange3_Previews: PreviewProvider {
    static var previews: some View {
        challange3()
    }
}
