//
//  day12:13:14.swift
//  100daysofswiftui
//
//  Created by hiba on 29/8/2022.
//

import SwiftUI

struct day12_13_14: View {
    var body: some View {
        ZStack {
            
            VStack (spacing : 70){
                VStack(spacing: 8){
                    Text("#100DaysOfSwiftUi")
                        .font(.system(size: 40))
                        .fontWeight(.bold)
                        .foregroundColor(Color(hue: 1.0, saturation: 0.923, brightness: 0.974, opacity: 0.718))
                    HStack {
                        Image(systemName: "star.fill")
                        Text("Day12/13/14")
                            .font(.system(size: 40))
                        Image(systemName: "star.fill")
                    }
                }
                Image(systemName: "swift")
                    .font(.system(size: 150))
                    .foregroundColor(Color(hue: 1.0, saturation: 0.923, brightness: 0.974, opacity: 0.718))
                
                
                Text("""
        classes,
        inheritance,
        protocols,
        extensions,
        optionals,
        and nil coalescing
        """)
                .font(.system(size: 30))
                .multilineTextAlignment(.center)
                .lineSpacing(4)
                
                
            }
        }
    }
}

struct day12_13_14_Previews: PreviewProvider {
    static var previews: some View {
        day12_13_14()
    }
}
