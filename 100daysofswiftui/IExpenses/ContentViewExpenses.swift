//
//  ContentViewExpenses.swift
//  iExpense
//
//


//day38
//learn about @StateObject, sheet(), onDelete()

import SwiftUI
struct StyleOfAmount: ViewModifier {
    var amount: Int
    func body(content: Content) -> some View {
        var font = Font.system(size: 22, weight: .heavy, design: .default)
        var foregroundColor = Color.black
        if amount < 10 {
            foregroundColor = Color.red
        } else if amount == 10 || amount < 100 {
            foregroundColor = Color.black
//            font = Font.system(size: 25, weight: .medium, design: .monospaced)
        } else {
            foregroundColor = Color.gray
//            font = Font.system(size: 30, weight: .bold, design: .rounded)

        }
        return content
            .foregroundColor(foregroundColor)
//            .font(font)
    }
}

extension View {
    func setStyleForAmount(_ amount: Int) -> some View {
        self.modifier(StyleOfAmount(amount: amount))
    }
}
struct ContentViewExpenses: View {
    @StateObject var expenses = Expenses()
    @State private var showingAddExpense = false

    var body: some View {
        NavigationView {
            VStack {
                List{
                        ForEach(expenses.items) { item in
                            HStack {
                                            VStack(alignment: .leading) {
                                                Text(item.name)
                                                    .font(.headline)
                                                Text(item.type)
                                            }

                                            Spacer()
                                       
                                            Text(item.amount, format: .currency(code: item.currency))
                                                .setStyleForAmount(Int(item.amount))
                                    }
                                    .padding()
                                    .border(item.type == "Personal" ? .blue : .red)

                        
                        }
                        .onDelete(perform: removeItems)
                }
            }
            .navigationTitle("iExpense")
            .toolbar {
                Button {
                    showingAddExpense = true
                } label: {
                    Image(systemName: "plus")
                }
            }
            .sheet(isPresented: $showingAddExpense) {
                AddView(expenses: expenses)
            }
        }
    }

    func removeItems(at offsets: IndexSet) {
        expenses.items.remove(atOffsets: offsets)
    }
}

struct ContentViewExpenses_Previews: PreviewProvider {
    static var previews: some View {
        ContentViewExpenses()
    }
}



/*


}*/
