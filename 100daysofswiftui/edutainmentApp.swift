//
//  edutainmentApp.swift
//  100daysofswiftui
//
//  Created by hiba on 31/8/2022.
//
//day35

import SwiftUI

struct edutainmentApp: View {
    @State private var selectedNumber = 0
    @State private var showingAnimation = false
    @State private var scoreTitle = ""
    @State private var showingScore = false
    @State private var startview =  true
    @State private var nbMultip = 0
    @State private var nbQuastion = 0
    @State private var  yourTransformers = 0
    @State private var arrNb = [1,2,3,4,5,6,7,8,9,10,11,12].shuffled()
    @State private var arrRandom = [1,2,3,4].shuffled()
    @State private var score = 0
    @State private var correctAnswer = Int.random(in: 1...4)

    let rows = [
        GridItem(.flexible() , spacing: 30),
        GridItem(.flexible(), spacing: 30),
        GridItem(.flexible(), spacing: 30)
    ]
    var body: some View {
        if startview {
            ZStack{
                Image("bagImg")
                    .resizable()
                    .frame(maxWidth: .infinity , maxHeight: .infinity)
                    .edgesIgnoringSafeArea(.all)
                
                VStack (spacing: 50){
                    Spacer()
                    Spacer()
                    Spacer()
                    //the muiltiple number
                    VStack(spacing: 20){
                        Text("1 X ?")
                            .font(.title2)
                            .fontWeight(.bold)
                            .foregroundColor(Color(red: 0.411, green: 0.223, blue: 0.011))
                        Section {
                            LazyHGrid(rows: rows, spacing: 15){
                                ForEach(1..<13){ nb in
                                    Button(action: {
                                        nbMultip  = nb
                                    }, label: {
                                        Text("\(nb)")
                                            .frame(width:35 , height:35)
                                            .background()
                                            .cornerRadius(50)
                                            .foregroundColor(.black)
                                    })

                                }
                            }
                            
                        }
                    }
                   //number of quations
                    VStack{
                        Text(" number of questions")
                            .font(.title2)
                            .fontWeight(.bold)
                            .foregroundColor(Color(red: 0.411, green: 0.223, blue: 0.011))
                        
                        Section {
                            HStack(spacing: 10){
                                ForEach(5..<13){ nb in
                                    Button(action: {
                                        nbQuastion  = nb
                                    }, label: {
                                        Text("\(nb)")
                                            .frame(width:35 , height: 35)
                                            .background()
                                            .cornerRadius(50)
                                            .foregroundColor(.black)
                                    })
                                    
                                }
                            }
                        }
                    }
                    Spacer()
                    //start button
                    Button(action: {
                        startview = false
                    }, label: {
                        ZStack{
                            Circle()
                                .foregroundColor(.white)
                                .frame(width:200,height: 100)
                            Text("START")
                                .font(.title2)
                                .fontWeight(.bold)
                                .foregroundColor(Color(red: 0.411, green: 0.223, blue: 0.011))
                        }
                          
                     
                          
                    })
                    Spacer()
                    Spacer()
                }
                .padding()
                
            }
        }
        else{
            ZStack{
                Image("bagImg2")
                    .resizable()
                    .frame(maxWidth: .infinity , maxHeight: .infinity)
                    .edgesIgnoringSafeArea(.all)
                
                VStack (){
                    Spacer()
                    Text("\(nbMultip) X \(arrNb[correctAnswer]) = ??")
                        .font(.system(size: 60))
                        .foregroundColor(.white)
                        .padding(.top, 100)
                    Spacer()
                    HStack{
                        ForEach (0..<arrRandom.count) { i in
                            Button {
                                withAnimation {
                                    nextQuation(i)
                                   // askQuestion()
                                }


                            } label: {
                                Text("\(arrNb[arrRandom[i]] * (nbMultip))")
                                    .fontWeight(.bold)
                                    .font(.title2)
                                    .foregroundColor(.white)
                                    .frame(width:60 , height:60)
                                    .background(Color(red: 0.574, green: 0.283, blue: 0.102))
                                    .cornerRadius(20)

                            }
                            .rotation3DEffect(.degrees(self.showingAnimation ? 360 : 0), axis: (x: 0, y: 1, z: 0))

                        }
                    }
                    Spacer()
                    
                }
                
            }
           .alert(scoreTitle, isPresented: $showingScore) {
                if yourTransformers == nbQuastion {
                    Button("Reset", action: ResetFunc)
                    Button("restart" , action: restart)
                }else{
                    Button("Continue", action: askQuestion)
                }
            }
        }
        
    }
    func restart(){
        startview = true
        yourTransformers = 0
        score = 0
    }
    
      func askQuestion() {
        arrNb = [1,2,3,4,5,6,7,8,9,10,11,12].shuffled()
        arrRandom = [1,2,3,4].shuffled()
        correctAnswer = Int.random(in: 1...4)
        showingAnimation = false
    }
    
    func ResetFunc () {
        arrNb = [1,2,3,4,5,6,7,8,9,10,11,12].shuffled()
        correctAnswer = Int.random(in: 1...4)
        yourTransformers = 0
        score = 0
    }

    
     func nextQuation(_ i:Int) {
         selectedNumber = i
         yourTransformers += 1
        if yourTransformers < nbQuastion {
            if( (arrNb[arrRandom[i]] * nbMultip) == ( nbMultip * arrNb[correctAnswer]) ){
                score += 1
                scoreTitle = "correct"
                showingAnimation = true
            }else{
                scoreTitle = "wrong"
            }
        }else{
            if score >= (nbQuastion/2) {
                scoreTitle = "you win \(score)/\(nbQuastion)"
            }else{
                scoreTitle = "you lose \(score)/\(nbQuastion) "
            }
        }
         showingScore = true
    }
    
    
}

struct edutainmentApp_Previews: PreviewProvider {
    static var previews: some View {
        edutainmentApp()
    }
}
