//
//  5:6.swift
//  100daysofswiftui
//
//  Created by hiba on 19/8/2022.
//

import SwiftUI

struct day5_6: View {
    var body: some View {
        ZStack {
      
            VStack (spacing : 70){
                VStack(spacing: 8){
                Text("#100DaysOfSwiftUi")
                        .font(.system(size: 40))
                        .fontWeight(.bold)
                        .foregroundColor(Color(hue: 0.079, saturation: 0.754, brightness: 0.931))
                    HStack {
                        Image(systemName: "star.fill")
                        Text("Day5/6")
                            .font(.system(size: 40))
                        Image(systemName: "star.fill")
                    }
                }
                Image(systemName: "swift")
                    .font(.system(size: 150))
                    .foregroundColor(Color(hue: 0.079, saturation: 0.754, brightness: 0.931))
          
                       
                    Text("""
        if,
        switch,
        the ternary operator,
        and loops
        """)
                    .font(.system(size: 30))
                    .multilineTextAlignment(.center)
                    .lineSpacing(4)
                    
                
            }
        }
    }
}

struct day5_6_Previews: PreviewProvider {
    static var previews: some View {
        day5_6()
    }
}
