//
//  challange1.swift
//  100daysofswiftui
//
//  Created by hiba on 23/8/2022.
//day15

import SwiftUI

struct challange1: View {
    @State private var useRedText = false
    @State private var time: Double = 60
    @State private var fromtipTime: String = "H"
    @State private var totipTime: String = "H"
    let tipTimes = [ "H" ,"M"  ,"S"]
    var converty : Double {
        var res = time
        if (fromtipTime == "H"){
          
            if (totipTime == "M"){
                res = time * 60
            }
            if (totipTime == "S"){
                res = time * 3600
            }
        }
        
        
        if (fromtipTime == "S"){
            if (totipTime == "M"){
                res = time * 0.0166667
            }
            if (totipTime == "H"){
                res = time * 0.000277778
            }

        }
        

        
        if (fromtipTime == "M"){
            if (totipTime == "S"){
                res = time * 60
            }
            if (totipTime == "H"){
                res = time * 0.0166667
            }
        }
        
        return res
    }
    var body: some View {
        NavigationView {
            Form{
                
                Section{
                    TextField("Time" ,value: $time, format:.number)
                } header: {
                    Text("write your time")
                }
                Section {
                
               
                        Picker("time",selection: $fromtipTime){
                            ForEach(tipTimes, id: \.self){
                                Text($0)
                           
                            }
                        }
                        .pickerStyle(.segmented)
                        .colorMultiply(.yellow)
                    
                } header: {
                    Text("Convert your time from")
                }
                
                Section {
                    Picker("time",selection: $totipTime){
                        ForEach(tipTimes, id: \.self){
                            Text($0)
                        }
                    }
                    .pickerStyle(.segmented)
                    .colorMultiply(.yellow)
                } header: {
                    Text("Convert your time to ")
                }
                
                Section{
                    Text(" \(converty) \(totipTime.lowercased())")
                        .foregroundColor(converty > 1 ? .black : .red )
                }
            }
            .navigationTitle("Converty App")
            .navigationBarTitleDisplayMode(.inline)
        }
   
    }
}

struct challange1_Previews: PreviewProvider {
    static var previews: some View {
        challange1()
    }
}


/*import SwiftUI

struct challange1: View {
    @State var selectedUnits = 0
    @State private var input = 100.0
    @State private var inputUnit: Dimension = UnitLength.meters
    @State private var outputUnit: Dimension = UnitLength.yards
    @FocusState private var inputIsFocused: Bool
    
    let conversions = ["Distance", "Mass", "Temperature", "Time"]
    
    let unitTypes = [
        [UnitLength.meters, UnitLength.kilometers, UnitLength.feet, UnitLength.yards, UnitLength.miles],
        [UnitMass.grams, UnitMass.kilograms, UnitMass.ounces, UnitMass.pounds],
        [UnitTemperature.celsius, UnitTemperature.fahrenheit, UnitTemperature.kelvin],
        [UnitDuration.hours, UnitDuration.minutes, UnitDuration.seconds]
    ]
    
 
    let formatter: MeasurementFormatter
    init() {
        formatter = MeasurementFormatter()
        formatter.unitOptions = .providedUnit
        formatter.unitStyle = .long
    }
    var result: String {
        let inputLength  = Measurement(value: input, unit: inputUnit)
        let outputLength = inputLength.converted(to: outputUnit)
        return formatter.string(from: outputLength)
    }
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    TextField("Amount", value: $input, format: .number)
                        .keyboardType(.decimalPad)
                        .focused($inputIsFocused)
                } header: {
                    Text("Amount to convert")
                }
                
                Picker("Conversion", selection: $selectedUnits) {
                    ForEach(0..<conversions.count) {
                        Text(conversions[$0])
                    }
                }
                    
                Picker("Convert from", selection: $inputUnit) {
                    ForEach(unitTypes[selectedUnits], id: \.self) {
                        Text(formatter.string(from: $0).capitalized)
                    }
                }
                .pickerStyle(.segmented)
                .colorMultiply(.yellow)
                Picker("Convert to", selection: $outputUnit) {
                    ForEach(unitTypes[selectedUnits], id: \.self) {
                        Text(formatter.string(from: $0).capitalized)
                    }
                }.pickerStyle(.segmented)
                    .colorMultiply(.yellow)
                
                Section {
                    Text(result)
                } header: {
                    Text("Result")
                }
            }
            .navigationTitle("Converter")
            .toolbar {
                ToolbarItemGroup(placement: .keyboard) {
                    Button("Done") {
                        inputIsFocused = false
                    }
                }
            }
            .onChange(of: selectedUnits) { newSelection in
                let units = unitTypes[newSelection]
                inputUnit = units[0]
                outputUnit = units[1]
            }
        }
    }
}

struct challange1_Previews: PreviewProvider {
    static var previews: some View {
        challange1()
    }
}
*/



