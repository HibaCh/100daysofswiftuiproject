//
//  HabitTrackingAppView.swift
//  100daysofswiftui

//  Created by hiba on 9/9/2022.
//

//day47
import SwiftUI

struct HabitTrackingAppView: View {
    @ObservedObject var habits = Habits()
    @State private var showingAddExpense = false

    var body: some View {
        NavigationView {
            VStack {
                List{
                    ForEach(habits.habitIt) { item in
                           // HStack {
                                NavigationLink(destination: HabitDiscView(habits: habits, habit: HabitItem( name: item.name, Discrption: item.Discrption, count: item.count)), label: {
                                                        Text(item.name)
                                                            .font(.headline)
                                    Spacer()
                                    
                                    Text("\(item.count)")
                                        .padding(.trailing)
                                })
                                   // }
                                    .padding(10)
                                   

                        
                        }
                        .onDelete(perform: removeItems)
                }
            }
            .navigationTitle("Activites")
            .toolbar {
                Button {
                    showingAddExpense = true
                } label: {
                    Image(systemName: "plus")
                }
            }
            .sheet(isPresented: $showingAddExpense) {
                AddActivitesView(habits: habits)
            }
        
        }
    }

    func removeItems(at offsets: IndexSet) {
        habits.habitIt.remove(atOffsets: offsets)
    }
}

struct HabitTrackingAppView_Previews: PreviewProvider {
    static var previews: some View {
        HabitTrackingAppView()
    }
}
