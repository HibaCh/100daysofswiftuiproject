//
//  Habits.swift
//  100daysofswiftui
//
//  Created by hiba on 9/9/2022.
//

import Foundation
class Habits: ObservableObject {
    @Published var habitIt = [HabitItem]() {
        didSet {
            if let encoded = try? JSONEncoder().encode(habitIt) {
                UserDefaults.standard.set(encoded, forKey: "habit")
            }
        }
     

    }

    init() {
        if let savedHabit = UserDefaults.standard.data(forKey: "habit") {
            if let decodedHabit = try? JSONDecoder().decode([HabitItem].self, from: savedHabit) {
                habitIt = decodedHabit
                return
            }
        }

        habitIt = []
    }
}

