//
//  HabitDiscView.swift
//  100daysofswiftui
//
//  Created by hiba on 9/9/2022.
//

import SwiftUI

struct HabitDiscView: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var habits: Habits
    var habit: HabitItem
    @State  var completedTimes = 0

    
    
    var body: some View {
        VStack(spacing:100){
            
            Spacer()
            Text(habit.name)
                .font(.title)
            Text(habit.Discrption)
            HStack(spacing:30){
                
                Button(action: {
                    
                    
                    
                    
                    
                    
                    completedTimes += 1
                    
                }, label: {
                    Image(systemName: "plus")
                        .foregroundColor(.black)
                        .padding()
                        .background(Color(hue: 0.616, saturation: 0.374, brightness: 0.914))
                        .cornerRadius(50)
                })
                
                
                
                Text("\(completedTimes)")
                
                
                Button(action: {
                  
                    completedTimes -= 1
                }, label: {
                    Image(systemName: "minus")
                        .foregroundColor(.black)
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                        .padding()
                        .padding(.vertical,6)
                        .background(Color(hue: 0.616, saturation: 0.374, brightness: 0.914))
                        .cornerRadius(50)
                })
                
            }
            Spacer()
            Spacer()
        }
        .navigationBarItems(trailing: Button(action: {
                   self.saveActivity()
            self.presentationMode.wrappedValue.dismiss()
               }, label: {
                   Text("Save")
               }))
        .onAppear{
            completedTimes = habit.count
        }
      
    }
    
    
    func saveActivity(){
        if let indexItem = self.habits.habitIt.firstIndex(where: { (item) -> Bool in item.name == habit.name}){
            let tempActivity = HabitItem( name: self.habit.name, Discrption: self.habit.Discrption, count: completedTimes)
            self.habits.habitIt.remove(at: indexItem)
            self.habits.habitIt.insert(tempActivity, at: indexItem)
          //  self.habits.reloadData()
    }
    }
    
    
}

struct HabitDiscView_Previews: PreviewProvider {
    static var previews: some View {
        HabitDiscView(habits: Habits(), habit: HabitItem( name:  "Name", Discrption: "Description", count: 0))
    }
}

