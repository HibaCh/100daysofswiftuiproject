//
//  AddActivitesView.swift
//  100daysofswiftui
//
//  Created by hiba on 9/9/2022.
//

import SwiftUI

struct AddActivitesView: View {
    @ObservedObject var habits: Habits
    @Environment(\.dismiss) var dismiss
    @State private var name = ""
    @State private var discription = ""
    var body: some View {
        NavigationView {
            Form {
                TextField("Name", text: $name)
                TextField("discription", text: $discription)
            }
            .navigationTitle("Add new habit")
            .toolbar {
                ToolbarItem(placement: ToolbarItemPlacement.navigationBarTrailing){
                    Button("Save") {
                        let item = HabitItem(name: name, Discrption:discription, count: 0 )
                        habits.habitIt.append(item)
                        dismiss()
                    }
                }
           
            }
         
        }
        }
}

struct AddActivitesView_Previews: PreviewProvider {
    static var previews: some View {
        AddActivitesView(habits: Habits())
    }
}
