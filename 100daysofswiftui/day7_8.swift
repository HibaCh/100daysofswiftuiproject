//
//  day7:8.swift
//  100daysofswiftui
//
//  Created by hiba on 15/8/2022.
//

import SwiftUI

struct day7_8: View {
    var body: some View {
        ZStack {
      
            VStack (spacing : 70){
                VStack(spacing: 8){
                Text("#100DaysOfSwiftUi")
                        .font(.system(size: 40))
                        .fontWeight(.bold)
                        .foregroundColor(Color(hue: 0.75, saturation: 0.433, brightness: 0.977))
                    HStack {
                        Image(systemName: "star.fill")
                        Text("Day7/8")
                            .font(.system(size: 40))
                        Image(systemName: "star.fill")
                    }
                }
                Image(systemName: "swift")
                    .font(.system(size: 150))
                    .foregroundColor(Color(hue: 0.75, saturation: 0.433, brightness: 0.977))
          
                       
                    Text("""
        functions,
        parameters,
        return values
        default values,
        and throwing functions
        """)
                    .font(.system(size: 30))
                    .multilineTextAlignment(.center)
                    .lineSpacing(4)
                    
                
            }
        }
    }
}

struct day7_8_Previews: PreviewProvider {
    static var previews: some View {
        day7_8()
    }
}
