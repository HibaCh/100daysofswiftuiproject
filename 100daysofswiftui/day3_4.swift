//
//  day3.swift
//  100daysofswiftui
//
//  Created by hiba on 12/8/2022.
//
import SwiftUI

struct day3_4: View {
    var body: some View {
        ZStack {
      
            VStack (spacing : 70){
                VStack(spacing: 8){
                Text("#100DaysOfSwiftUi")
                        .font(.system(size: 40))
                        .fontWeight(.bold)
                        .foregroundColor(Color(hue: 0.276, saturation: 0.544, brightness: 0.909))
                    HStack {
                        Image(systemName: "star.fill")
                        Text("Day3/4")
                            .font(.system(size: 40))
                        Image(systemName: "star.fill")
                    }
                }
                Image(systemName: "swift")
                    .font(.system(size: 150))
                    .foregroundColor(Color(hue: 0.276, saturation: 0.544, brightness: 0.909))
          
                       
                    Text("""
        Arrays,
        dictionaries,
        sets,
        enums,
        and type annotations
        """)
                    .font(.system(size: 30))
                    .multilineTextAlignment(.center)
                    .lineSpacing(4)
                    
                
            }
        }
    }
}

struct day3_4_Previews: PreviewProvider {
    static var previews: some View {
        day3_4()
    }
}

