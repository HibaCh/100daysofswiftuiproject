//
//  ListLayout.swift
//  100daysofswiftui
//
//  Created by hiba on 8/9/2022.
//

import SwiftUI


    struct ListLayout: View {
        let missions :[Mission]
        let astronauts : [String: Astronaut]
        var body: some View {
                List{
                    ForEach(missions) { mission in
                        NavigationLink {
                            MissionView(mission: mission, astronauts: astronauts)
                        } label: {
                          HStack {
                                Image(mission.image)
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 70, height: 70)
                                    .padding()
                                VStack {
                                    Text(mission.displayName)
                                        .font(.headline)
                                        .foregroundColor(.white)

                                    Text(mission.formattedLaunchDate)
                                        .font(.caption)
                                        .foregroundColor(.white.opacity(0.5))
                                }
                                .padding(.vertical)
                                .frame(maxWidth: .infinity)
                               
                            }

                        }
                        
                    }
                }
                
            
           
        }
    }

    struct ListLayout_Previews: PreviewProvider {
        static let missions: [Mission] = Bundle.main.decode("missions.json")
        static let astronauts: [String: Astronaut] = Bundle.main.decode("astronauts.json")
        static var previews: some View {
            ListLayout(missions: missions, astronauts: astronauts)
           
        }
    }


