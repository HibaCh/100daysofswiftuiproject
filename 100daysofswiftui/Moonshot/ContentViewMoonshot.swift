//
//  ContentView.swift
//  Moonshot
//day42
// GeometryReader, ScrollView, NavigationLink...

import SwiftUI

struct ContentViewMoonshot: View {
    @State private var showingGrid = true
    let astronauts: [String: Astronaut] = Bundle.main.decode("astronauts.json")
      let missions: [Mission] = Bundle.main.decode("missions.json")

    var body: some View {
        NavigationView {
            Group {
                if showingGrid {
                    ListLayout(missions: missions, astronauts: astronauts)
                  } else {
               
                    GridLayout(missions: missions, astronauts: astronauts)
                  }
            }
            .navigationTitle("Moonshot")
            .toolbar {
                Button(action:{
                    showingGrid ? ( showingGrid = false) : ( showingGrid = true)
                    print(showingGrid)
                }, label: {
                    Image(systemName: showingGrid ? "square.grid.2x2" : "list.dash" )
                        .foregroundColor(.white)
                })
            }

        }
        .background(.darkBackground)
        .preferredColorScheme(.dark)
    }
}

struct ContentViewMoonshot_Previews: PreviewProvider {
    static var previews: some View {
        ContentViewMoonshot()
    }
}
